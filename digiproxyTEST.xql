xquery version "3.0";
import module namespace config="http://exist-db.org/xquery/apps/config" at "/db/apps/SADE/core/config.xqm";
import module namespace tgclient="http://textgrid.info/namespaces/xquery/tgclient" at "/db/apps/textgrid-connect/tgclient.xqm";

let $project := 'textgrid'
let $config := map { "config" := config:config($project) }

let $sid := tgclient:getSidCached($config)
let $reqUrl := config:param-value($config, "textgrid.digilib") || "/"

let $id := 'C07_002.jpg'
let $id :=  if (matches($id, '_') and (starts-with($id, 'A') or starts-with($id, 'B') or starts-with($id, 'C') or starts-with($id, 'D') or starts-with($id, 'E')) )
                then (doc('/db/sade-projects/' || $project || '/images.xml')//object[@type="image/jpeg"][@title = $id][last()]/substring-before(@uri, '.'))[last()]
                else $id
let $reqUrl := 'http://textgrid-esx1.gwdg.de/1.0/digilib/rest/digilib/'
let $req := <http:request href="{concat($reqUrl,$id,";sid=",$sid,"?dh=200")}" method="get" timeout="0">
                <http:header name="connection" value="close"/>
            </http:request>

let $result := http:send-request($req)

return
    $result